import React, { Component } from "react";
import { dataShoe } from "./dataShoe";
import ListShoe from "./ListShoe";
import DetailShoe from "./DetailShoe";
import Cart from "./Cart";

export default class Ex_ShoeShop extends Component {
  state = {
    shoeArr: dataShoe,
    detail: dataShoe[1],
    cart: [],
  };
  handleChangeDetail = (value) => {
    this.setState({ detail: value });
  };

  handleAddToCart = (shoe) => {
    let cloneCart = [...this.state.cart];
    cloneCart.push(shoe);
    this.setState({
      cart: cloneCart,
    });
  };
  render() {
    return (
      <div className="container">
        <Cart cart={this.state.cart} />
        <ListShoe
          handleAddToCart={this.handleAddToCart}
          handleChangeDetail={this.handleChangeDetail}
          shoeArr={this.state.shoeArr}
        />
        <DetailShoe detail={this.state.detail} />
      </div>
    );
  }
}
