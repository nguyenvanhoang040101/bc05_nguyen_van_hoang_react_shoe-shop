import React, { Component } from "react";

export default class ItemShoe extends Component {
  render() {
    let { image, name } = this.props.data;
    return (
      <div className="col-3 p-1">
        <div className="card text-left h-100">
          <img className="card-img-top" src={image} alt />
          <div className="card-body">
            <h4 className="card-title">{name}</h4>
            <button
              onClick={() => {
                this.props.handleAddToCart(this.props.data);
              }}
              className="btn btn-success mr-5"
            >
              Buy
            </button>
            {/* // ham co tham so nen la arrow function */}
            <button
              onClick={() => {
                this.props.handleViewDetail(this.props.data);
              }}
              className="btn btn-danger"
            >
              Detail
            </button>
          </div>
        </div>
      </div>
    );
  }
}
